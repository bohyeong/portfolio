(window => {
  const modal = document.querySelector('#posts-modal');
  const modalContent = document.querySelector("#posts-modal .modal-content");

  const close = () => {
    modal.scrollTop = 0;
    modal.classList.remove("show");
    document.body.classList.remove("modal-open");
  };
  const open = () => {
    modal.classList.add("show");
    document.body.classList.add("modal-open");
  }

  modalContent.addEventListener("click", (event) => {
    if (event.target == modalContent) {
      close();
    }
  });

  document.querySelector("#posts-modal .close")
    .addEventListener("click", close);

  let slideIndex = 1;
  const nextSlide = n => showSlides(slideIndex += n);
  const currentSlide = n => showSlides(slideIndex = n);

  const slides = document.querySelectorAll(".posts-slides");
  document.querySelector("#posts-modal a.prev")
    .addEventListener("click", () => nextSlide(-1));
  document.querySelector("#posts-modal a.next")
    .addEventListener("click", () => nextSlide(1));

  const showSlides = (n) => {
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (let i = 0; i < slides.length; i++) {
      slides[i].classList.remove("show");
    }
    slides[slideIndex - 1].classList.add("show");
  }

  document.addEventListener("keydown", (event) => {
    if (event.key === "Escape") {
      close();
    } else if (event.key === "ArrowLeft") {
      nextSlide(-1);
    } else if (event.key === "ArrowRight") {
      nextSlide(1);
    }
  });

  const posts = document.querySelectorAll("#posts .item");
  posts.forEach((post, index) => {
    post.addEventListener("click", () => {
      open();
      currentSlide(index + 1);
    });
  });
})(window);
