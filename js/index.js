(function (window) {
  const navtop = document.querySelector(".navtop");
  const apply = () => {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
      navtop.classList.add("opaque");
    } else {
      navtop.classList.remove("opaque");
    }
  }

  window.addEventListener("scroll", apply);
  window.addEventListener("DOMContentLoaded", apply);
  window.addEventListener("load", () => document.body.classList.add("loaded"));
})(window);

(function (window) {
  var btns = document.querySelectorAll(".btn.filter");
  var filter = function (query) {
    btns.forEach(b => {
      var data = b.dataset['filter'];
      if (data == query) {
        b.classList.add('active');
      } else {
        b.classList.remove('active');
      }
    });

    var posts = document.querySelectorAll('#posts > .item');
    posts.forEach(p => {
      if (p.classList.contains(query) || query == '*' || query == '') {
        p.classList.remove('hidden');
      } else {
        p.classList.add('hidden');
      }
    });
  };


  btns.forEach(b => {
    b.onclick = function () {
      filter(b.dataset['filter']);
    };
  });
})(window);

/* Top Button */
(window => {
  const btn = document.querySelector("#top-btn");
  btn.addEventListener("click", () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  });
  const apply = () => {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
      btn.classList.remove("hidden");
    } else {
      btn.classList.add("hidden");
    }
  }
  window.addEventListener("scroll", apply);
  window.addEventListener("DOMContentLoaded", apply);
})(window);

/* Gallery */
(window => {
  const imgs = document.querySelectorAll(".gallery-wrap .gallery-img");

  const init = () => {
    imgs.forEach((img) => {
      img.classList.remove("active");
    });
  };

  imgs.forEach((img) => {
    img.addEventListener("click", () => {
      if (!img.classList.contains("active")) {
        init();
        img.classList.add("active");
      } else {
        init();
      }
    });
  });
})(window);
