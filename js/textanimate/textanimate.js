(function(window) {
  class Scheduler {
    constructor() {
      this.delay = 0;
      this.end = new Date().getTime();
    }

    next(func, delay) {
      var params = Array.prototype.slice.call(arguments, 2);
      var current = new Date().getTime();
      if (current > this.end) {
        this.end = current;
      }
      setTimeout(func, this.end-current+delay, ...params);
      this.end += delay
    }
  }

  class TextAnimate extends Scheduler {
    constructor(contentSel, contents, cursorSel) {
      super();

      this.contentEl = document.querySelector(contentSel);

      this.cursorEl = document.querySelector(cursorSel);
      this.cursorEl.transition = ".4s";

      this.state = {
        contents: contents,
        index: 0
      }
    }

    appear(index) {
      this.state['index'] = index;
      var content = this.state["contents"][index];
      for (var i = 0; i < content.length; i++) {
        this.next((c) => {
          this.contentEl.textContent += c;
        }, 80, content[i]);
      }
    }

    disappear() {
      var content = this.state["contents"][this.state["index"]];
      for (var i = 0; i < content.length; i++) {
        this.next(() => {
          this.contentEl.textContent = this.contentEl.textContent.slice(0, -1);
        }, 80);
      }
    }

    blink() {
      for (var i = 0; i < 4; i++) {
        this.next(() => {
          this.cursorEl.style.opacity = 0;
        }, 400);
        this.next(() => {
          this.cursorEl.style.opacity = 1;
        }, 400);
      }
    }

    start() {
      this.contentEl.textContent = "";

      for (var i = 0; i < this.state["contents"].length*10; i++) {
        this.appear(i%this.state["contents"].length);
        this.blink();
        this.disappear();
      }

      this.appear((this.state["index"]+1)%this.state["contents"].length);
    }
  }

  var textanimate = function(contentSel, contents, cursorSel) {
    new TextAnimate(contentSel, contents, cursorSel).start();
  }

  window.textanimate = textanimate;
})(window);
