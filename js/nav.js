(function (window) {
  function isInViewport(element) {
    if (element == null) {
      return false;
    }
    const rect = element.getBoundingClientRect();
    const center = (window.innerHeight || document.documentElement.clientHeight) / 2
    return rect.top < center && rect.bottom > center;
  }

  const sidebar = document.querySelector("#mobile-menu-sidebar");
  const overlay = document.querySelector("#mobile-menu-overlay");
  const open = () => {
    sidebar.style.display = "block";
    overlay.style.display = "block";
  };
  const close = () => {
    sidebar.style.display = "none";
    overlay.style.display = "none";
  }

  document.querySelector(".open-button").addEventListener("click", open)
  document.querySelector(".close-button").addEventListener("click", close);
  document.querySelector("#mobile-menu-overlay").addEventListener("click", close);

  const links = document.querySelectorAll(".navtop nav a");
  const mlinks = document.querySelectorAll(".mobile-sidebar > a");

  const clicked = link => {
    const v = document.querySelector("#" + link.name);
    link.addEventListener("click", () => {
      v.scrollIntoView();
    })
  };
  links.forEach(clicked);
  mlinks.forEach(clicked);
  mlinks.forEach(link => {
    link.addEventListener("click", close);
  })

  const sections = []
  links.forEach(a => {
    sections.push(document.querySelector("#" + a.name));
  });

  window.addEventListener('scroll', () => {
    for (let i = 0; i < sections.length; i++) {
      const section = sections[i];
      if (isInViewport(section)) {
        links.forEach(a => {
          if (section.id == a.name) {
            a.querySelector("span").classList.add("active");
          } else {
            a.querySelector("span").classList.remove("active");
          }
        });

        break;
      }
    }
  });

})(window);
